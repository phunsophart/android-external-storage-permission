package com.example.admin.localstoragedemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG ="data" ;
    EditText username, password;
    Button btnSave, btnShow, btnR, btnW, btnSaveFile, btnShowFile;
    Context context;

    String filename ="myFile.txt";
    String total="";

    String fileName="externalfile.txt";
    File myExternalFile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = findViewById(R.id.edUser);
        password = findViewById(R.id.edPass);
        btnSave = findViewById(R.id.btnSave);
        btnShow = findViewById(R.id.btnShow);
        btnR = findViewById(R.id.btnRead);
        btnW = findViewById(R.id.btnWrite);
        btnSaveFile = findViewById(R.id.btnSaveFile);
        btnShowFile = findViewById(R.id.btnShowFile);

        if (isExternalFileWritable() && isExternalFileAvaible()){ //use && instead off ||
            btnSaveFile.setEnabled(true);
            Log.e(LOG_TAG, "writeable->" + isExternalFileWritable() + " and readable->" + isExternalFileAvaible());
        }else{
            myExternalFile = getPublicAlbumStorageDir("MY_FOLDER"); //not file name but folder name
            Log.e(LOG_TAG, "get into else not writable and readable " + myExternalFile.getAbsolutePath());
        }


        btnSaveFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!hasRuntimePermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        requestinPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
                    } else {
                        Log.e(LOG_TAG,"Get into else: Permission granted");
                        File myFile = new File(myExternalFile,fileName);
                    FileOutputStream outputStream = new FileOutputStream(myFile);
                        Log.e(LOG_TAG,"File created in: ->" + myFile.getAbsolutePath());
                    outputStream.write(username.getText().toString().getBytes());
                    outputStream.write(password.getText().toString().getBytes());
                    outputStream.close();
                    username.setText("");
                    password.setText("");
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream fileOutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                    fileOutputStream.write(username.getText().toString().getBytes());
                    fileOutputStream.write(password.getText().toString().getBytes());
                    fileOutputStream.close();
                    username.setText("");
                    password.setText("");

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });



        btnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fileInputStream = openFileInput(filename);
                    InputStreamReader reader = new InputStreamReader(fileInputStream);
                    BufferedReader bufferedReader = new BufferedReader(reader);
                    int data=0;
                    while ((data=reader.read())!=-1){
                     total+=data+"\t";
                    }
                    username.setText(total);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences share = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = share.edit();
                editor.putString("username", username.getText().toString());
                editor.putString("password", password.getText().toString());
                editor.commit();
                username.setText("");
                password.setText("");
                Toast.makeText(getApplicationContext(),"Save Success", Toast.LENGTH_SHORT).show();
            }
        });

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
                username.setText(sharedPreferences.getString("username",null));
                password.setText(sharedPreferences.getString("password", null));
            }
        });


    }

    public static boolean isExternalFileAvaible(){
        String extStorage = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorage)){
            return true;
        }
        return false;
    }

    public static boolean isExternalFileWritable(){
        String extStorageWrite = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageWrite)){
            return true;
        }
        return false;
    }

    boolean hasRuntimePermission(Context context, String runtimePersmission){

        boolean value= false;
        int currentDeviceVersion = Build.VERSION.SDK_INT;
        if (currentDeviceVersion > Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(context, runtimePersmission) == PackageManager.PERMISSION_GRANTED){
                value = true;
            }
        }else {
            value = true;
        }
        return value;
    }

    void requestinPermission(Activity activity, String runtimepermission, int requestion_code){
        ActivityCompat.requestPermissions(activity,new String[]{runtimepermission}, requestion_code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    public File getPublicAlbumStorageDir(String dirName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), dirName);
        if (!file.mkdirs()) {
            Log.e(LOG_TAG, "Directory not created");
        }else{
            Log.e(LOG_TAG, "Directory created" + Environment.DIRECTORY_DCIM);
        }
        return file;
    }
}
